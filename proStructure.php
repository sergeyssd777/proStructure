<?php
/*v3.1 bitrix custom*/
class ProStructure
{
    private $unic;
    public function __construct($DataArr, $OrderBy = 'ID',$unic='ID')
    {
        $this->DataArr = array();
        $this->unic = $unic;
        if (!empty($DataArr) && is_array($DataArr)) {
            if (empty($OrderBy)) {
                $this->DataArr = $DataArr;
            } else {
                foreach ($DataArr as $Item) {
                    $this->DataArr[$Item[$OrderBy]] = $Item;
                }
            }
        }
        return $this;
    }
    //Create struct by inner KEY
    public function createStruct($KeyParam='IBLOCK_SECTION_ID',$StructName='Struct',$OutOptions=array()){
        if(empty($this->DataArr)){
            $this->$StructName=array();
            return $this->$StructName;
        }
        $KeyParam=(string)$KeyParam;
        $DataArr=array();//array after filtering
        /**/
        $Options=array();
        $Options['ExcludeElemsArr']=array();//array id to exclude
        $Options['ExcludeParamValues']=array();//array parameter var to exclude (common child ids)
        $Options['SortedElemsAssocBy']=$this->unic; //ID
        $Options['ParentField']=$this->unic; //ID
        /**/
        if(count($OutOptions)){
            foreach($OutOptions as $Key=>$Value){
                switch($Key){
                    case 'ExcludeElemsArr':
                    case 'ExcludeParamValues':
                    case 'SortedElemsAssocBy':
                    case 'ParentField':
                        $Options[$Key]=$Value;
                        break;
                }
            }
            unset($OutOptions);
        }
        foreach($this->DataArr as &$Elem){
            if(!in_array($Elem[$this->unic],$Options['ExcludeElemsArr'])&&!in_array($Elem[$KeyParam],$Options['ExcludeParamValues'])){
                $DataArr[$Elem[$this->unic]]=&$Elem;
            }
            unset($Elem);
        }
        foreach($DataArr as &$Elem){
            //связываем ссылками элементы
            $sortingKey=!empty($Options['SortedElemsAssocBy'])?$Options['SortedElemsAssocBy']:$this->unic; //сортировочный ключ
            // если не пустое поле родитель и родитель есть в результирующем списке
            if(!empty($Elem[$KeyParam])&&!empty($DataArr[$Elem[$KeyParam]])){
                if(empty($DataArr[$Elem[$KeyParam]]['Childs']))$DataArr[$Elem[$KeyParam]]['Childs']=[];
                $DataArr[$Elem[$KeyParam]]['Childs'][$sortingKey]=$Elem;
            }elseif(empty($Elem[$KeyParam])){
                //иначе отправляем на верхний уровень
                $DataArr[$sortingKey]=&$Elem;
            }
            //все что ссылается на несуществующего родителя в фильтре игнорим
        }
        $this->$StructName=$DataArr;
        return $this->$StructName;
    }
    //Sorting
    public function SortByParam($KeyParam='IBLOCK_SECTION_ID',$StructName='Sort',$OutOptions=array()){
        if(empty($this->DataArr)){
            $this->Sort=array();
            return array();
        }
        $DataArr=array();
        $OutStruct=array();
        /**/
        $Options=array();
        $Options['ExcludeElemsArr']=array();
        $Options['ExcludeParamValues']=array();
        $Options['DateTimeMonth']=false;//sorting by month from datestamp
        $Options['SortedElemsAssocBy']='';
        /**/
        if(count($OutOptions)){
            foreach($OutOptions as $Key=>$Value){
                if(!empty($Value)){
                    switch($Key){
                        case 'ExcludeElemsArr':
                        case 'ExcludeParamValues':
                        case 'DateTimeMonth':
                        case 'SortedElemsAssocBy':
                            $Options[$Key]=$Value;
                            break;
                    }
                }
            }
            unset($OutOptions);
        }
        if(!empty($this->DataArr)){
            foreach($this->DataArr as $Elem){
                if(!in_array($Elem['ID'],$Options['ExcludeElemsArr'])&&!in_array($Elem[$KeyParam],$Options['ExcludeParamValues'])){
                    $DataArr[]=$Elem;
                }
                unset($Elem);
            }
        }
        foreach($DataArr as $Elem){
            switch($KeyParam){
                case 'DateTime':
                    if($Options['DateTimeMonth']){
                        $UnicKey=$Elem->Month.'_'.$Elem->Year;
                        if(empty($OutStruct[$UnicKey])){
                            $OutStruct[$UnicKey]=array();
                        }
                        if(!empty($Options['SortedElemsAssocBy'])){
                            $OutStruct[$UnicKey][$Elem->$Options['SortedElemsAssocBy']]=$Elem;
                        }
                        else{
                            $OutStruct[$UnicKey][]=$Elem;
                        }
                    }
                    break;
                default:
                    if(empty($OutStruct[$Elem[$KeyParam]])){
                        $OutStruct[$Elem[$KeyParam]]=array();
                    }
                    if(!empty($Options['SortedElemsAssocBy'])){
                        $OutStruct[$Elem[$KeyParam]][$Elem[$Options['SortedElemsAssocBy']]]=$Elem;
                    }
                    else{
                        $OutStruct[$Elem[$KeyParam]][]=$Elem;
                    }
                    break;
            }
            unset($Elem);
        }
        $this->$StructName=$OutStruct;
        return $this->$StructName;
    }
    //Ext fork by links in DataArr not with Obj clones
    public function getChildIds($BaseId,$AddSelf=false,$StructureName='Struct'){
        $IdsOut=array();
        if(empty($BaseId)||empty($this->$StructureName))
            return array();
        if(empty($this->DataArr[$BaseId]))
            return -1;
        if(!empty($this->DataArr[$BaseId]['Childs'])){
            foreach($this->DataArr[$BaseId]['Childs'] as $Child){
                $this->getChildId($IdsOut,$Child);
            }
        }
        if($AddSelf) $IdsOut[]=$BaseId;
        return $IdsOut;
    }
    private function getChildId(&$Ids,$Child){
        $Ids[]=$Child['ID'];
        if(!empty($Child['Childs'])){
            foreach($Child['Childs'] as $SubChild){
                $this->getChildId($Ids,$SubChild);
            }
        }
    }
    private function printItem($Obj,$Prefix=''){
        ?>
        <option
            value="<?=$Obj['ID']?>" <?=(!empty($_GET['categoryId'])&&$_GET['categoryId']==$Obj['ID']?'selected':'')?>><?=$Prefix.$Obj['NAME']?></option>
        <?
        if(!empty($Obj['Childs'])){
            $Prefix.='-';
            foreach($Obj['Childs'] as $Child){
                $this->printItem($Child,$Prefix);
            }
        }
    }
    public function printTree($StructName='Struct'){
        if(!empty($this->$StructName)){
            foreach($this->$StructName as $Obj){
                $this->printItem($Obj);
            }
        }
    }
    private function getParentId(&$Ids,$Parent){
        $Ids[]=$Parent['ID'];
        if(!empty($this->DataArr[$Parent['IBLOCK_SECTION_ID']])){
            $this->getParentId($Ids,$this->DataArr[$Parent['IBLOCK_SECTION_ID']]);
        }
    }
    public function getParentIds($BaseId,$StructureName='Struct'){
        $IdsOut=array();
        if(empty($BaseId)||empty($this->$StructureName))
            return array();
        if(empty($this->DataArr[$BaseId]))
            return -1;
        if(!empty($this->DataArr[$this->DataArr[$BaseId]['IBLOCK_SECTION_ID']])){
            $this->getParentId($IdsOut,$this->DataArr[$this->DataArr[$BaseId]['IBLOCK_SECTION_ID']]);
        }

        return $IdsOut;
    }
}